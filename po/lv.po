# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
#
#
# Rūdofls Mazurs <rudolfs.mazurs@gmail.com>, 2011, 2012, 2013.
# Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>, 2013, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-d"
"ocuments&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-08-23 15:36+0000\n"
"PO-Revision-Date: 2017-08-28 11:31+0200\n"
"Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <lata-l10n@googlegroups.com>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 :"
" 2);\n"
"X-Generator: Lokalize 2.0\n"

#: data/org.gnome.Books.appdata.xml.in:7 data/org.gnome.Books.desktop.in:3
#: src/application.js:112 src/overview.js:1052
msgid "Books"
msgstr "Grāmatas"

#: data/org.gnome.Books.appdata.xml.in:8
msgid "An e-book manager application for GNOME"
msgstr "E-grāmatu pārvaldības lietotne GNOME vidē"

#: data/org.gnome.Books.appdata.xml.in:10
msgid ""
"A simple application to access and organize your e-books on GNOME. It is "
"meant to be a simple and elegant replacement for using a file manager to "
"deal with e-books."
msgstr ""
"Vienkārša lietotne e-grāmatu piekļūšanai, organizēšanai un koplietošanai "
"GNOME vidē. Tā ir paredzēta kā datņu pārvaldnieka aizvietotājs darbam ar e-"
"grāmatām."

#: data/org.gnome.Books.appdata.xml.in:15
#: data/org.gnome.Documents.appdata.xml.in:16
msgid "It lets you:"
msgstr "Tās jums ļauj:"

#: data/org.gnome.Books.appdata.xml.in:17
msgid "View recent e-books"
msgstr "Skatīt nesenās e-grāmatas"

#: data/org.gnome.Books.appdata.xml.in:18
msgid "Search through e-books"
msgstr "Meklēt e-grāmatās"

#: data/org.gnome.Books.appdata.xml.in:19
msgid "View e-books (PDF and comics) fullscreen"
msgstr "Skatīt e-grāmatas (PDF un komiksus) pilnekrāna režīmā"

#: data/org.gnome.Books.appdata.xml.in:20
msgid "Print e-books"
msgstr "Drukāt e-grāmatas"

#: data/org.gnome.Books.appdata.xml.in:32
#: data/org.gnome.Documents.appdata.xml.in:39
msgid "The GNOME Project"
msgstr "GNOME projekts"

#: data/org.gnome.Books.desktop.in:4
msgid "Access, manage and share books"
msgstr "Piekļūt, pārvaldīt un koplietot e-grāmatas"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.Books.desktop.in:7
msgid "org.gnome.Books"
msgstr "org.gnome.Books"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Books.desktop.in:15
msgid "Books;Comics;ePub;PDF;"
msgstr "Grāmatas;Komiksi;ePub;PDF;"

#: data/org.gnome.books.gschema.xml:5 data/org.gnome.documents.gschema.xml:5
msgid "View as"
msgstr "Skatīt kā"

#: data/org.gnome.books.gschema.xml:6 data/org.gnome.documents.gschema.xml:6
msgid "View as type"
msgstr "Skatīt kā tipu"

#: data/org.gnome.books.gschema.xml:10 data/org.gnome.documents.gschema.xml:10
msgid "Sort by"
msgstr "Kārtot pēc"

#: data/org.gnome.books.gschema.xml:11 data/org.gnome.documents.gschema.xml:11
msgid "Sort by type"
msgstr "Kārtot pēc tipa"

#: data/org.gnome.books.gschema.xml:15 data/org.gnome.documents.gschema.xml:15
msgid "Window size"
msgstr "Loga izmērs"

#: data/org.gnome.books.gschema.xml:16 data/org.gnome.documents.gschema.xml:16
msgid "Window size (width and height)."
msgstr "Loga izmērs (platums un augstums)."

#: data/org.gnome.books.gschema.xml:20 data/org.gnome.documents.gschema.xml:20
msgid "Window position"
msgstr "Loga novietojums"

#: data/org.gnome.books.gschema.xml:21 data/org.gnome.documents.gschema.xml:21
msgid "Window position (x and y)."
msgstr "Loga novietojums (x un y)."

#: data/org.gnome.books.gschema.xml:25 data/org.gnome.documents.gschema.xml:25
msgid "Window maximized"
msgstr "Logs maksimizēts"

#: data/org.gnome.books.gschema.xml:26 data/org.gnome.documents.gschema.xml:26
msgid "Window maximized state"
msgstr "Loga stāvoklis — maksimizēts"

#: data/org.gnome.books.gschema.xml:30 data/org.gnome.documents.gschema.xml:30
msgid "Night mode"
msgstr "Nakts režīms"

#: data/org.gnome.books.gschema.xml:31 data/org.gnome.documents.gschema.xml:31
msgid "Whether the application is in night mode."
msgstr "Vai lietotne ir nakts režīmā."

#: data/org.gnome.Documents.appdata.xml.in:7
#: data/org.gnome.Documents.desktop.in:3 src/application.js:115
#: src/overview.js:1052
msgid "Documents"
msgstr "Dokumenti"

#: data/org.gnome.Documents.appdata.xml.in:8
msgid "A document manager application for GNOME"
msgstr "Dokumentu pārvaldības lietotne GNOME vidē"

#: data/org.gnome.Documents.appdata.xml.in:10
msgid ""
"A simple application to access, organize and share your documents on GNOME. "
"It is meant to be a simple and elegant replacement for using a file manager "
"to deal with documents. Seamless cloud integration is offered through GNOME "
"Online Accounts."
msgstr ""
"Vienkārša lietotne, kas noder dokumentu piekļūšanai, organizēšanai un "
"koplietošanai GNOME vidē. Tā ir paredzēta kā datņu pārvaldnieka aizvietotājs "
"darbam ar dokumentiem. Integrāciju ar mākoņpakalpojumiem nodrošina caur "
"GNOME tiešsaistes kontiem."

#: data/org.gnome.Documents.appdata.xml.in:18
msgid "View recent local and online documents"
msgstr "Skatiet nesenos lokālos un tiešsaistes dokumentus"

#: data/org.gnome.Documents.appdata.xml.in:19
msgid "Access your Google, ownCloud or OneDrive content"
msgstr "Piekļūstiet savam Google, ownCloud vai OneDrive saturam"

#: data/org.gnome.Documents.appdata.xml.in:20
msgid "Search through documents"
msgstr "Meklējiet dokumentos"

#: data/org.gnome.Documents.appdata.xml.in:21
msgid "See new documents shared by friends"
msgstr "Skatiet jaunus dokumentus, ar kuriem dalījušies draugi"

#: data/org.gnome.Documents.appdata.xml.in:22
msgid "View documents fullscreen"
msgstr "Skatiet dokumentus pilnekrāna režīmā"

#: data/org.gnome.Documents.appdata.xml.in:23
msgid "Print documents"
msgstr "Drukājiet dokumentus"

#: data/org.gnome.Documents.appdata.xml.in:24
msgid "Select favorites"
msgstr "Izvēlēties izlasi"

#: data/org.gnome.Documents.appdata.xml.in:25
msgid "Allow opening full featured editor for non-trivial changes"
msgstr "Ļauj atvērt redaktoru netriviālām izmaiņām"

#: data/org.gnome.Documents.desktop.in:4
msgid "Access, manage and share documents"
msgstr "Piekļūt dokumentiem, pārvaldīt un koplietot tos"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.Documents.desktop.in:7
#| msgid "Documents"
msgid "org.gnome.Documents"
msgstr "org.gnome.Documents"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Documents.desktop.in:15
msgid "Docs;PDF;Document;"
msgstr "Dokuments;PDF;"

#: data/ui/app-menu.ui:6 src/preview.js:453
msgid "Night Mode"
msgstr "Nakts režīms"

#: data/ui/app-menu.ui:12
msgid "Keyboard Shortcuts"
msgstr "Tastatūras saīsnes"

#: data/ui/app-menu.ui:16
msgid "Help"
msgstr "Palīdzība"

#: data/ui/app-menu.ui:20
msgid "About"
msgstr "Par"

#: data/ui/app-menu.ui:24
msgctxt "app menu"
msgid "Quit"
msgstr "Iziet"

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Vispārēji"

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "Rādīt palīdzību"

#: data/ui/help-overlay.ui:22
msgctxt "shortcut window"
msgid "Quit"
msgstr "Iziet"

#: data/ui/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Search"
msgstr "Meklēt"

#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Print the current document"
msgstr "Izdrukāt pašreizējo dokumentu"

#: data/ui/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Navigācija"

#: data/ui/help-overlay.ui:49 data/ui/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Go back"
msgstr "Iet atpakaļ"

#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Preview"
msgstr "Priekšskatījums"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Tuvināt"

#: data/ui/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Tālināt"

#: data/ui/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Bookmark the current page"
msgstr "Pievienot lappusi grāmatzīmēm"

#: data/ui/help-overlay.ui:92
msgctxt "shortcut window"
msgid "Open places and bookmarks dialog"
msgstr "Atvērt vietu un grāmatzīmju dialoglodziņu"

#: data/ui/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Copy selected text to clipboard"
msgstr "Kopēt izvēlēto starpliktuvē"

#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Rotate anti-clockwise"
msgstr "Pagriezt pretēji pulksteņrādītāja virzienam"

#: data/ui/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Rotate clockwise"
msgstr "Pagriezt pulksteņrādītāja virzienā"

#: data/ui/help-overlay.ui:120
msgctxt "shortcut window"
msgid "Next occurrence of the search string"
msgstr "Nākamā virknes parādīšanās"

#: data/ui/help-overlay.ui:127
msgctxt "shortcut window"
msgid "Previous occurrence of the search string"
msgstr "Iepriekšējā meklētās virknes parādīšanās"

#: data/ui/help-overlay.ui:134
msgctxt "shortcut window"
msgid "Presentation mode"
msgstr "Prezentācijas režīms"

#: data/ui/help-overlay.ui:141
msgctxt "shortcut window"
msgid "Open action menu"
msgstr "Atvērt darbības izvēlni"

#: data/ui/help-overlay.ui:148
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Pilnekrāns"

#: data/ui/organize-collection-dialog.ui:40
msgid "Enter a name for your first collection"
msgstr "Ievadiet savas pirmās kolekcijas nosaukumu"

#: data/ui/organize-collection-dialog.ui:56
#: data/ui/organize-collection-dialog.ui:113
msgid "New Collection…"
msgstr "Jauna kolekcija…"

#: data/ui/organize-collection-dialog.ui:73
#: data/ui/organize-collection-dialog.ui:122 src/sharing.js:226
msgid "Add"
msgstr "Pievienot"

#: data/ui/organize-collection-dialog.ui:167 src/overview.js:616
msgid "Cancel"
msgstr "Atcelt"

#. Label for Done button in Sharing dialog
#: data/ui/organize-collection-dialog.ui:173 src/sharing.js:112
msgid "Done"
msgstr "Gatavs"

#: data/ui/preview-context-menu.ui:6
msgid "_Copy"
msgstr "_Kopēt"

#. Translators: this is the Open action in a context menu
#: data/ui/preview-menu.ui:6 data/ui/selection-toolbar.ui:9
#: src/selections.js:988
msgid "Open"
msgstr "Atvērt"

#: data/ui/preview-menu.ui:10
msgid "Edit"
msgstr "Rediģēt"

#: data/ui/preview-menu.ui:15
msgid "Print…"
msgstr "Drukāt…"

#: data/ui/preview-menu.ui:21 src/preview.js:462
msgid "Fullscreen"
msgstr "Pilnekrāns"

#: data/ui/preview-menu.ui:27
msgid "Present"
msgstr "Parādīt"

#: data/ui/preview-menu.ui:35
msgid "Zoom In"
msgstr "Tuvināt"

#: data/ui/preview-menu.ui:41
msgid "Zoom Out"
msgstr "Tālināt"

#: data/ui/preview-menu.ui:49
msgid "Rotate ↶"
msgstr "Pagriezt ↶"

#: data/ui/preview-menu.ui:55
msgid "Rotate ↷"
msgstr "Pagriezt ↷"

#: data/ui/preview-menu.ui:62 data/ui/selection-toolbar.ui:79
#: src/properties.js:61
msgid "Properties"
msgstr "Īpašības"

#: data/ui/selection-menu.ui:6
msgid "Select All"
msgstr "Izvēlēties visus"

#: data/ui/selection-menu.ui:11
msgid "Select None"
msgstr "Neizvēlēties nevienu"

#: data/ui/selection-toolbar.ui:88 src/overview.js:1056 src/search.js:126
#: src/search.js:132
msgid "Collections"
msgstr "Kolekcijas"

#: data/ui/view-menu.ui:23
msgid "View items as a grid of icons"
msgstr "Skatīt vienumus kā ikonu režģi"

#: data/ui/view-menu.ui:40
msgid "View items as a list"
msgstr "Skatīt vienumus kā sarakstu"

#: data/ui/view-menu.ui:73
msgid "Sort"
msgstr "Kārtot"

#: data/ui/view-menu.ui:84
msgid "Author"
msgstr "Autors"

#: data/ui/view-menu.ui:93
msgid "Date"
msgstr "Datums"

#: data/ui/view-menu.ui:102
msgid "Name"
msgstr "Nosaukums"

#: src/application.js:126
msgid "Show the version of the program"
msgstr "Rādīt programmas versiju"

#: src/documents.js:794
msgid "Failed to print document"
msgstr "Neizdevās izdrukāt dokumentu"

#. Translators: this refers to local documents
#: src/documents.js:836 src/search.js:459
msgid "Local"
msgstr "Lokāls"

#. Translators: Documents ships a "Getting Started with Documents"
#. tutorial PDF. The "GNOME" string below is displayed as the author name
#. of that document, and doesn't normally need to be translated.
#: src/documents.js:879
msgid "GNOME"
msgstr "GNOME"

#: src/documents.js:880
msgid "Getting Started with Documents"
msgstr "Darba sākšana ar Dokumentiem"

#: src/documents.js:905 src/documents.js:1115 src/documents.js:1218
#: src/documents.js:1412
msgid "Collection"
msgstr "Kolekcija"

#. overridden
#: src/documents.js:972
msgid "Google Docs"
msgstr "Google Docs"

#: src/documents.js:973
msgid "Google"
msgstr "Google"

#: src/documents.js:1117 src/documents.js:1414
msgid "Spreadsheet"
msgstr "Izklājlapa"

#: src/documents.js:1119 src/documents.js:1416 src/presentation.js:44
msgid "Presentation"
msgstr "Prezentācija"

#: src/documents.js:1121 src/documents.js:1418
msgid "e-Book"
msgstr "e-grāmata"

#: src/documents.js:1123 src/documents.js:1420
msgid "Document"
msgstr "Dokuments"

#. overridden
#: src/documents.js:1173
msgid "ownCloud"
msgstr "ownCloud"

#. overridden
#: src/documents.js:1307 src/documents.js:1308
msgid "OneDrive"
msgstr "OneDrive"

#: src/documents.js:1594
msgid "Please check the network connection."
msgstr "Lūdzu, pārbaudiet tīkla savienojumu."

#: src/documents.js:1597
msgid "Please check the network proxy settings."
msgstr "Lūdzu, pārbaudiet tīkla starpnieka iestatījumus."

#: src/documents.js:1600
msgid "Unable to sign in to the document service."
msgstr "Nevar ierakstīties dokumentu servisā."

#: src/documents.js:1603
msgid "Unable to locate this document."
msgstr "Neizdevās atrast šo dokumentu."

#: src/documents.js:1606
#, javascript-format
msgid "Hmm, something is fishy (%d)."
msgstr "Hmm, kaut kas nav tā (%d)."

#: src/documents.js:1613
msgid ""
"You are using a preview of Books. Full viewing capabilities are coming soon!"
msgstr ""
"Jūs izmantojat Grāmatu priekšskatījumu. Drīz būs pieejamas pilnas iespējas!"

#: src/documents.js:1615
msgid ""
"LibreOffice support is not available. Please contact your system "
"administrator."
msgstr ""
"Nav pieejams LibreOffice atbalsts. Sazinieties ar sistēmas administratoru."

#. Translators: %s is the title of a document
#: src/documents.js:1638
#, javascript-format
msgid "Oops! Unable to load “%s”"
msgstr "Ak vai! Neizdevās ielādēt “%s”"

#. view button, on the right of the toolbar
#: src/edit.js:139
msgid "View"
msgstr "Skats"

#: src/epubview.js:228
#, javascript-format
#| msgid "Page %u of %u"
msgid "chapter %s of %s"
msgstr "nodaļa %s no %s"

#: src/evinceview.js:590 src/lib/gd-places-bookmarks.c:656
msgid "Bookmarks"
msgstr "Grāmatzīmes"

#: src/evinceview.js:598
msgid "Bookmark this page"
msgstr "Pievienot lappusi grāmatzīmēm"

#: src/lib/gd-nav-bar.c:242
#, c-format
msgid "Page %u of %u"
msgstr "Lappuse %u no %u"

#: src/lib/gd-pdf-loader.c:142
msgid "Unable to load the document"
msgstr "Neizdevās ielādēt dokumentu"

#. Translators: %s is the number of the page, already formatted
#. * as a string, for example "Page 5".
#.
#: src/lib/gd-places-bookmarks.c:321
#, c-format
msgid "Page %s"
msgstr "Lappuse %s"

#: src/lib/gd-places-bookmarks.c:384
msgid "No bookmarks"
msgstr "Nav grāmatzīmju"

#: src/lib/gd-places-bookmarks.c:392 src/lib/gd-places-links.c:257
msgid "Loading…"
msgstr "Ielādē…"

#: src/lib/gd-places-links.c:342
msgid "No table of contents"
msgstr "Nav satura rādītāja"

#: src/lib/gd-places-links.c:514
msgid "Contents"
msgstr "Saturs"

#: src/lib/gd-utils.c:328
msgid "A document manager application"
msgstr "Dokumentu pārvaldības lietotne"

#: src/lib/gd-utils.c:333
msgid "An e-books manager application"
msgstr "E-grāmatu pārvaldības lietotne"

#: src/lib/gd-utils.c:340
msgid "translator-credits"
msgstr ""
"Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>\n"
"Anita Reitere <nitalynx@gmail.com>"

#: src/mainToolbar.js:91
msgctxt "toolbar button tooltip"
msgid "Search"
msgstr "Meklēt"

#: src/mainToolbar.js:100
msgid "Back"
msgstr "Atpakaļ"

#. Translators: only one item has been deleted and %s is its name
#: src/notifications.js:48
#, javascript-format
msgid "“%s” deleted"
msgstr "“%s” izdzēsts"

#. Translators: one or more items might have been deleted, and %d
#. is the count
#: src/notifications.js:52
#, javascript-format
msgid "%d item deleted"
msgid_plural "%d items deleted"
msgstr[0] "Izdzēsts %d vienums"
msgstr[1] "Izdzēsti %d vienumi"
msgstr[2] "Izdzēsti %d vienumi"

#: src/notifications.js:61 src/selections.js:387
msgid "Undo"
msgstr "Atsaukt"

#: src/notifications.js:159
#, javascript-format
msgid "Printing “%s”: %s"
msgstr "Drukā “%s” — %s"

#: src/notifications.js:215
msgid "Your documents are being indexed"
msgstr "Dokumenti tiek indeksēti"

#: src/notifications.js:216
msgid "Some documents might not be available during this process"
msgstr "Šajā laikā daži dokumenti varētu nebūt pieejami"

#. Translators: %s refers to an online account provider, e.g.
#. "Google", or "Windows Live".
#: src/notifications.js:238
#, javascript-format
msgid "Fetching documents from %s"
msgstr "Saņem dokumentus no %s"

#: src/notifications.js:240
msgid "Fetching documents from online accounts"
msgstr "Saņem dokumentus no tiešsaistes kontiem"

#: src/overview.js:275
msgid "No collections found"
msgstr "Nav atrastu kolekciju"

#: src/overview.js:277
msgid "No books found"
msgstr "Nav atrastu grāmatu"

#: src/overview.js:277
msgid "No documents found"
msgstr "Nav atrastu dokumentu"

#: src/overview.js:286
msgid "Try a different search"
msgstr "Mēģiniet ar citu meklējamo vārdu"

#: src/overview.js:293
msgid "You can create collections from the Books view"
msgstr "Varat izveidot kolekcijas no grāmatu skata"

#: src/overview.js:295
msgid "You can create collections from the Documents view"
msgstr "Varat izveidot kolekcijas no dokumentu skata"

#: src/overview.js:305
#, javascript-format
msgid ""
"Documents from your <a href=\"system-settings\">Online Accounts</a> and <a "
"href=\"file://%s\">Documents folder</a> will appear here."
msgstr ""
"Dokumenti no <a href=\"system-settings\">tiešsaistes kontiem</a> un <a href="
"\"file://%s\">dokumentu mapes</a> parādīsies šeit."

#. Translators: this is the menu to change view settings
#: src/overview.js:562
msgid "View Menu"
msgstr "Skata izvēlne"

#: src/overview.js:590
msgid "Click on items to select them"
msgstr "Spiediet uz vienumiem, lai tos izvēlētos"

#: src/overview.js:592
#, javascript-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "%d izvēlēts"
msgstr[1] "%d izvēlēti"
msgstr[2] "%d izvēlētu"

#: src/overview.js:669
msgid "Select Items"
msgstr "Izvēlēties vienumus"

#: src/overview.js:876
msgid "Yesterday"
msgstr "Vakar"

#: src/overview.js:878
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "Pirms %d dienas"
msgstr[1] "Pirms %d dienām"
msgstr[2] "Pirms %d dienām"

#: src/overview.js:882
msgid "Last week"
msgstr "Pagājušajā nedēļā"

#: src/overview.js:884
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "Pirms %d nedēļas"
msgstr[1] "Pirms %d nedēļām"
msgstr[2] "Pirms %d nedēļām"

#: src/overview.js:888
msgid "Last month"
msgstr "Pagājušajā mēnesī"

#: src/overview.js:890
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "Pirms %d mēneša"
msgstr[1] "Pirms %d mēnešiem"
msgstr[2] "Pirms %d mēnešiem"

#: src/overview.js:894
msgid "Last year"
msgstr "Pagājušajā gadā"

#: src/overview.js:896
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "Pirms %d gada"
msgstr[1] "Pirms %d gadiem"
msgstr[2] "Pirms %d gadiem"

#: src/password.js:42
msgid "Password Required"
msgstr "Nepieciešama parole"

#: src/password.js:45
msgid "_Unlock"
msgstr "_Atbloķēt"

#: src/password.js:61
#, javascript-format
msgid "Document %s is locked and requires a password to be opened."
msgstr "Dokuments %s ir bloķēts; lai to atvērtu, ir vajadzīga parole."

#: src/password.js:75
msgid "_Password"
msgstr "_Parole"

#: src/presentation.js:102
msgid "Running in presentation mode"
msgstr "Darbojas prezentācijas režīmā"

#: src/presentation.js:130
msgid "Present On"
msgstr "Parādīt"

#. Translators: "Mirrored" describes when both displays show the same view
#: src/presentation.js:166
msgid "Mirrored"
msgstr "Simetriski"

#: src/presentation.js:168
msgid "Primary"
msgstr "Primārais"

#: src/presentation.js:170
msgid "Off"
msgstr "Izslēgts"

#: src/presentation.js:172
msgid "Secondary"
msgstr "Sekundārais"

#. Translators: this is the Open action in a context menu
#: src/preview.js:445 src/selections.js:985
#, javascript-format
msgid "Open with %s"
msgstr "Atvērt ar %s"

#: src/preview.js:770
msgid "Find Previous"
msgstr "Meklēt iepriekšējo"

#: src/preview.js:776
msgid "Find Next"
msgstr "Meklēt nākamo"

#. Title item
#. Translators: "Title" is the label next to the document title
#. in the properties dialog
#: src/properties.js:81
msgctxt "Document Title"
msgid "Title"
msgstr "Nosaukums"

#. Translators: "Author" is the label next to the document author
#. in the properties dialog
#: src/properties.js:90
msgctxt "Document Author"
msgid "Author"
msgstr "Autors"

#. Source item
#: src/properties.js:97
msgid "Source"
msgstr "Avots"

#. Date Modified item
#: src/properties.js:103
msgid "Date Modified"
msgstr "Izmaiņu datums"

#: src/properties.js:110
msgid "Date Created"
msgstr "Izveidošanas datums"

#. Document type item
#. Translators: "Type" is the label next to the document type
#. (PDF, spreadsheet, ...) in the properties dialog
#: src/properties.js:119
msgctxt "Document Type"
msgid "Type"
msgstr "Tips"

#. Translators: "Type" refers to a search filter on the document type
#. (PDF, spreadsheet, ...)
#: src/search.js:120
msgctxt "Search Filter"
msgid "Type"
msgstr "Tips"

#. Translators: this refers to documents
#: src/search.js:123 src/search.js:260 src/search.js:453
msgid "All"
msgstr "Visi"

#: src/search.js:136
msgid "PDF Documents"
msgstr "PDF dokumenti"

#: src/search.js:143
msgid "e-Books"
msgstr "E-grāmatas"

#: src/search.js:147
msgid "Comics"
msgstr "Komikss"

#: src/search.js:152
msgid "Presentations"
msgstr "Prezentācijas"

#: src/search.js:155
msgid "Spreadsheets"
msgstr "Izklājlapas"

#: src/search.js:158
msgid "Text Documents"
msgstr "Teksta dokumenti"

#. Translators: this is a verb that refers to "All", "Title", "Author",
#. and "Content" as in "Match All", "Match Title", "Match Author", and
#. "Match Content"
#: src/search.js:257
msgid "Match"
msgstr "Atbilst"

#. Translators: "Title" refers to "Match Title" when searching
#: src/search.js:263
msgctxt "Search Filter"
msgid "Title"
msgstr "Nosaukums"

#. Translators: "Author" refers to "Match Author" when searching
#: src/search.js:266
msgctxt "Search Filter"
msgid "Author"
msgstr "Autors"

#. Translators: "Content" refers to "Match Content" when searching
#: src/search.js:269
msgctxt "Search Filter"
msgid "Content"
msgstr "Saturs"

#: src/search.js:449
msgid "Sources"
msgstr "Avoti"

#. Translators: the first %s is an online account provider name,
#. e.g. "Google". The second %s is the identity used to log in,
#. e.g. "foo@gmail.com".
#: src/search.js:508
#, javascript-format
msgid "%s (%s)"
msgstr "%s (%s)"

#: src/selections.js:360 src/selections.js:362
msgid "Rename…"
msgstr "Pārdēvēt…"

#: src/selections.js:366 src/selections.js:368
msgid "Delete"
msgstr "Dzēst"

#: src/selections.js:382
#, javascript-format
msgid "“%s” removed"
msgstr "“%s” ir izņemts"

#: src/selections.js:779
msgid "Rename"
msgstr "Pārdēvēt"

#. Translators: "Collections" refers to documents in this context
#: src/selections.js:785
msgctxt "Dialog Title"
msgid "Collections"
msgstr "Kolekcijas"

#: src/sharing.js:108
msgid "Sharing Settings"
msgstr "Koplietošanas iestatījumi"

#. Label for widget group for changing document permissions
#: src/sharing.js:145
msgid "Document permissions"
msgstr "Dokumenta atļaujas"

#. Label for permission change in Sharing dialog
#: src/sharing.js:152 src/sharing.js:330
msgid "Change"
msgstr "Mainīt"

#. Label for radiobutton that sets doc permission to private
#: src/sharing.js:176 src/sharing.js:305
msgid "Private"
msgstr "Privāts"

#: src/sharing.js:186 src/sharing.js:298
msgid "Public"
msgstr "Publisks"

#. Label for checkbutton that sets doc permission to Can edit
#: src/sharing.js:190 src/sharing.js:300
msgid "Everyone can edit"
msgstr "Jebkurš var rediģēt"

#. Label for widget group used for adding new contacts
#: src/sharing.js:197
msgid "Add people"
msgstr "Pievienot cilvēkus"

#. Editable text in entry field
#: src/sharing.js:204
msgid "Enter an email address"
msgstr "Ievadiet e-pasta adresi"

#: src/sharing.js:219 src/sharing.js:371
msgid "Can edit"
msgstr "Var rediģēt"

#: src/sharing.js:219 src/sharing.js:374
msgid "Can view"
msgstr "Var skatīt"

#: src/sharing.js:302
msgid "Everyone can read"
msgstr "Jebkurš var lasīt"

#: src/sharing.js:317
msgid "Save"
msgstr "Saglabāt"

#: src/sharing.js:368
msgid "Owner"
msgstr "Īpašnieks"

#: src/sharing.js:437
#, javascript-format
msgid "You can ask %s for access"
msgstr "Jūs varat prasīt %s piekļuvei"

#: src/sharing.js:474 src/sharing.js:510 src/sharing.js:567 src/sharing.js:584
#: src/sharing.js:603
msgid "The document was not updated"
msgstr "Dokuments netika augšupielādēts"

#: src/shellSearchProvider.js:285
msgid "Untitled Document"
msgstr "Nenosaukts dokuments"

#: src/trackerController.js:176
msgid "Unable to fetch the list of documents"
msgstr "Neizdevās saņemt dokumentu sarakstu"

#~ msgid "LibreOffice is required to view this document"
#~ msgstr "Ir vajadzīgs LibreOffice, lai skatītu šo dokumentu"

#~ msgid "Category"
#~ msgstr "Kategorija"

#~ msgid "Favorites"
#~ msgstr "Izlase"

#~ msgid "Shared with you"
#~ msgstr "Ar jums koplietots"
